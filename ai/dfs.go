package ai

import "container/list"

func Dfs(b *Board) (*Node, int) {
	node, expanded, _ := dfsLimit(b, -1)
	return node, expanded
}

func dfsLimit(b *Board, limit int) (*Node, int, bool) {

	todo := list.New()
	initialNode := &Node{b, nil, -1, 0}

	todo.PushBack(initialNode)

	visited:=make(map[string]bool)
	visited[b.toUglyString()] = true

	expandDirection := func(n *Node, dir int) *Node {
		brd := n.b.clone()
		if brd.move(dir) && !visited[brd.toUglyString()] {
			return &Node{brd, n, dir, n.count+1}
		}
		return nil
	}

	limitReached := false
	expanded := 0
	for element := todo.Back(); element != nil; element = todo.Back() {
		node := element.Value.(*Node)
		todo.Remove(element)

		expanded++

		if node.b.isGoal() {
			return node, expanded, limitReached
		}

		if limit != -1 && node.count >= limit {
			limitReached = true
			continue
		}

		var next *Node
		directions := [...]int{UP, LEFT, DOWN, RIGHT}

		for _, dir := range directions {
			next = expandDirection(node, dir)
			if next != nil {
				todo.PushBack(next)
				visited[next.b.toUglyString()] = true
			}			
		}
	}

	return nil, expanded, limitReached
}