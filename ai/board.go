package ai

import "strconv"


func NewBoard(size int, agent Position, blocks []Position) *Board {
	return &Board{size, blocks, agent, nil}
}

func (b *Board) move(direction int) bool {
	switch direction {
	case LEFT:
		return b.moveLeft()
	case RIGHT:
		return b.moveRight()
	case UP:
		return b.moveUp()
	case DOWN:
		return b.moveDown()
	}
	return false
}

func (b *Board) blockAt(pos Position) (*Position, int) {
	for ind, blk := range b.Blocks {
		if blk.X == pos.X && blk.Y == pos.Y {
			return &blk, ind
		}
	}
	return nil, -1
}

func (b *Board) isObstacleAt(pos Position) bool {
	for _, obs := range b.Obstacles {
		if obs == pos {
			return true
		}
	}
	return false
}

func (b *Board) moveLeft() bool {
	if b.Agent.X == 1 {
		return false
	}
	if (b.isObstacleAt(Position{b.Agent.X-1, b.Agent.Y})) {
		return false
	}
	b.Agent.X--
	if blk, ind:=b.blockAt(b.Agent); blk != nil {
		blk.X++
		b.Blocks[ind]=*blk
	}
	return true
}

func (b *Board) moveRight() bool {
	if b.Agent.X == b.Size {
		return false
	}
	if (b.isObstacleAt(Position{b.Agent.X+1, b.Agent.Y})) {
		return false
	}
	b.Agent.X++
	if blk, ind:=b.blockAt(b.Agent); blk != nil {
		blk.X--
		b.Blocks[ind]=*blk
	}
	return true
}

func (b *Board) moveUp() bool {
	if b.Agent.Y == 1 {
		return false
	}
	if (b.isObstacleAt(Position{b.Agent.X, b.Agent.Y-1})) {
		return false
	}
	b.Agent.Y--
	if blk, ind:=b.blockAt(b.Agent); blk != nil {
		blk.Y++
		b.Blocks[ind]=*blk
	}
	return true
}

func (b *Board) moveDown() bool {
	if b.Agent.Y == b.Size {
		return false
	}
	if (b.isObstacleAt(Position{b.Agent.X, b.Agent.Y+1})) {
		return false
	}
	b.Agent.Y++
	if blk, ind:=b.blockAt(b.Agent); blk != nil {
		blk.Y--
		b.Blocks[ind]=*blk
	}
	return true
}

func (b *Board) clone() *Board {
	newBoard := new(Board)
	newBoard.Agent = b.Agent
	newBoard.Size = b.Size
	newBoard.Blocks = make([]Position, len(b.Blocks))
	newBoard.Obstacles = make([]Position, len(b.Obstacles))
	for ind, blk := range b.Blocks {
		newBoard.Blocks[ind] = blk
	}
	for ind, obs := range b.Obstacles {
		newBoard.Obstacles[ind] = obs
	}
	return newBoard
}

func (b *Board) isGoal() bool {
	// fixed goal state
	col := 2
	blks := len(b.Blocks)
	row := b.Size - blks + 1

	if b.Size > 2 && (b.Agent.X != b.Size || b.Agent.Y != b.Size) {
		return false
	}

	// fmt.Printf("col %d, blocks %d, row %d", col, blks, row)

	for _, blk := range b.Blocks {
		if blk.X != col || blk.Y != row {
			return false
		}
		row++
	}

	return true
}

func (b *Board) equalsTo(b2 *Board) bool {
	if b.Size != b2.Size || b.Agent != b2.Agent {
		return false
	}
	// compare elements
	for ind, blk := range b.Blocks {
		blk2 := b2.Blocks[ind]
		if blk2 != blk {
			return false
		}
	}
	return true
}

func (b *Board) toUglyString() string {
	res := strconv.Itoa(b.Agent.X) + strconv.Itoa(b.Agent.Y)
	for _, blk := range b.Blocks {
		res = res + strconv.Itoa(blk.X) + strconv.Itoa(blk.Y)
	}
	return res
}

func (b *Board) ToString() string {
	out:=[][]byte{}

	out= make([][]byte, b.Size)

	for i:=0;i<b.Size;i++ {
		out[i] = make([]byte, b.Size)
		for j:=0;j<b.Size;j++ {
			out[i][j] = '.'
		}
	}

	for i, blk := range b.Blocks {
		out[blk.Y-1][blk.X-1] = letterFromIndex(i)
	}

	for _, obs := range b.Obstacles {
		out[obs.Y-1][obs.X-1] = '#'
	}

	out[b.Agent.Y-1][b.Agent.X-1] = 'X'

	var s string
	result := ""
	for i:=0;i<b.Size;i++ {
		s = string(out[i])
		result += s + "\n"
	}
	return result
}
