package ai

func Iddfs(b *Board) (*Node, int) {
	expanded := 0
	limit := 1
	node, exp, limitReached := dfsLimit(b, limit)
	expanded += exp
	for limitReached && node == nil {
		limit++
		node, exp, limitReached = dfsLimit(b, limit)
		expanded+=exp
	}
	return node, expanded
}