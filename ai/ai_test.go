package ai

import "testing"

// test letter from index
func TestLetterFromIndex(t *testing.T) {
	if "A" != letterFromIndex(0) {
		t.Fail()
	}
	if "B" != letterFromIndex(1) {
		t.Fail()
	}
}

// create a board, compare it to other board (identical and then different)

// test move possibilities:

// up, working
// up, not working
// up, working and moving a block
// --- same for other three directions

/*

board:

 1 2 3 4
1  
2
3C    
4  A B P

*/

func createMyBoard() *Board {
	return &Board{
		size: 4,
		agent: Position{4,4},
		blocks:[]Position{
			Position{2,4},
			Position{3,4},
			Position{1,3}}}
}

func TestEqualBoards(t *testing.T) {
	board1:=createMyBoard()
	board2:=createMyBoard()

	if !board1.equalsTo(board2) {
		t.Fail()
	}

	if !board2.equalsTo(board1) {
		t.Fail()
	}
}

func TestUnequalBoards(t *testing.T) {
	board1:=createMyBoard()
	board2:=&Board{
		size: 4,
		agent: Position{4,4},
		blocks:[]Position{
			Position{2,4},
			Position{3,4},
			Position{1,1}}}

	if board1.equalsTo(board2) {
		t.Fail()
	}

	board2=createMyBoard()
	board1.agent = Position{1,2}
	if board1.equalsTo(board2) {
		t.Fail()
	}
}

func TestMoveRight(t *testing.T) {
	board:=createMyBoard()
	board.agent = Position{2,1}

	if mv := board.moveRight(); mv == false {
		t.Fail()
	}

	pos := Position{3,1}

	if board.agent != pos {
		t.Fail()
	}

}

func TestMoveRightImpossible(t *testing.T) {
	board:=createMyBoard()
	pos := board.agent

	mv := board.moveRight()
	
	if mv {
		t.Fail()
	}

	if board.agent != pos {
		t.Fail()
	}
}

func TestMoveRightBlock(t *testing.T) {
	board:=createMyBoard()
	board.agent = Position{1,4}
	pos := Position{2,4}

	tmpBlk,index := board.blockAt(pos)

	if mv:=board.moveRight(); mv == false {
		t.Fail()
	}

	if pos != board.agent {
		t.Fail()
	}

	blk, ind := board.blockAt(board.agent)
	if *blk != *tmpBlk || ind != index {
		t.Fail()
	}
}

