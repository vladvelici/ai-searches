package main

import "./ai"
import "fmt"
import "os"
import "io/ioutil"
import "encoding/json"

func main() {
	algoIndex := map[string](func (*ai.Board) (*ai.Node, int)) {
		"dfs": ai.Dfs,
		"bfs": ai.Bfs,
		"iddfs": ai.Iddfs,
		"astar": ai.Astar}

	if len(os.Args) < 3 {
		fmt.Println("Need input file name and algorithm name.")
		os.Exit(1)
	}

	fileName := os.Args[1]
	algo := os.Args[2]

	csvFormat := false
	if len(os.Args) >= 4 && os.Args[3] == "--csv" {
		csvFormat = true
	}

	algoFunc, ok := algoIndex[algo]

	if !ok {
		fmt.Println("Algorithm not found. Try dfs, bfs or iddfs")
		os.Exit(1)
	}

	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Printf("File error: %v\n", err)
		os.Exit(1)
	}

	var start ai.Board
	jsonErr:=json.Unmarshal(file, &start)

	if jsonErr != nil {
		fmt.Printf("JSON Error: %v\n", jsonErr)
		os.Exit(1)
	}

	if !csvFormat {
		fmt.Println("Start state:")
		fmt.Println((&start).ToString())
	}

	finalNode, expanded := algoFunc(&start)

	if !csvFormat {
		fmt.Printf("Nodes expanded: %d\n", expanded)
		finalNode.PrintSolution()
	} else {
		fmt.Printf("%s,%s,%d,%d\n",fileName,algo,expanded,finalNode.GetCost())
	}
}
